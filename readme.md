# Bienvenue sur le dépôt Gitlab pour l'entraînement d'exercice en langage C.

Vous retrouverez chaque exercice sur chaque branche distincte. Il y a **10 exercices**.
<br>

Pour récupérer l'exercice 1, utilisez la commande suivante :
`git checkout -b Exercice_1 origin/Exercice_1`

Pour récupérer l'exercice 2, utilisez la commande suivante :
`git checkout -b Exercice_2 origin/Exercice_2`

Pour récupérer l'exercice 3, utilisez la commande suivante :
`git checkout -b Exercice_3 origin/Exercice_3`

Pour récupérer l'exercice 4, utilisez la commande suivante :
`git checkout -b Exercice_4 origin/Exercice_4`

Pour récupérer l'exercice 5, utilisez la commande suivante :
`git checkout -b Exercice_5 origin/Exercice_5`

Pour récupérer l'exercice 6, utilisez la commande suivante :
`git checkout -b Exercice_6 origin/Exercice_6`

Pour récupérer l'exercice 7, utilisez la commande suivante :
`git checkout -b Exercice_7 origin/Exercice_7`

Pour récupérer l'exercice 8, utilisez la commande suivante :
`git checkout -b Exercice_8 origin/Exercice_8`

Pour récupérer l'exercice 9, utilisez la commande suivante :
`git checkout -b Exercice_9 origin/Exercice_9`

Pour récupérer l'exercice 10, utilisez la commande suivante :
`git checkout -b Exercice_10 origin/Exercice_10`


Si vous rencontrez des problèmes, contactez-moi depuis le [site web](https://www.weboffyannctrofficiel.cf/contact/exercice-c).
